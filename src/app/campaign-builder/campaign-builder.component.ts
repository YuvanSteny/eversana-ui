import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { compareAsc, format } from 'date-fns'
@Component({
  selector: 'app-campaign-builder',
  templateUrl: './campaign-builder.component.html',
  styleUrls: ['./campaign-builder.component.scss']
})
export class CampaignBuilderComponent implements OnInit{

  campaignBuilderFormGroup!:FormGroup
  isSubmitted:boolean = false

  projectValueJson:any = [
    {
     "configure_project_id":"1",
    
     "company_name":"Jhonson",
    
     "brand_name":"Jhonson",
    
     "therapy_area":"Test",
    
     "is_delete":"N",
    
     "status":"A",
    
     "created_by":null,
    
     "created_date":"2023-04-05T07:00:51.580Z",
    
     "modified_by":null,
    
     "modified_date":"2023-04-05T07:00:51.580Z"
    },
    
    {
    
     "configure_project_id":"2",
    
     "company_name":"Margenza",
    
     "brand_name":"Margenza",
    
     "therapy_area":"Test",
    
     "is_delete":"N",
    
     "status":"A",
    
     "created_by":null,
    
     "created_date":"2023-04-05T07:00:51.580Z",
    
     "modified_by":null,
    
     "modified_date":"2023-04-05T07:00:51.580Z"
    
    },
    
    {
    
     "configure_project_id":"3",
    
     "company_name":"Jhonson&Jhonson",
    
     "brand_name":"Jhonson&Jhonson",
    
     "therapy_area":"Test",
    
     "is_delete":"N",
    
     "status":"A",
    
     "created_by":null,
    
     "created_date":"2023-04-05T07:00:51.580Z",
    
     "modified_by":null,
    
     "modified_date":"2023-04-05T07:00:51.580Z"
    
    }
    
    ]

  campaignObjectiveValueJson:any=[

    {
    
     "master_data_id":"2",
    
     "name":"TestObjectiver1",
    
     "description":"TestObjectiver1",
    
     "type":"OBJ1",
    
     "parent_master_id":"1",
    
     "is_delete":"N",
    
     "created_by":null,
    
     "created_date":"2023-04-05T06:46:29.387Z",
    
     "modified_by":null,
    
     "modified_date":"2023-04-05T06:46:29.387Z"
    
    },
    
    {
    
     "master_data_id":"3",
    
     "name":"TestObjectiver2",
    
     "description":"TestObjectiver2",
    
     "type":"OBJ2",
    
     "parent_master_id":"1",
    
     "is_delete":"N",
    
     "created_by":null,
    
     "created_date":"2023-04-05T06:50:59.517Z",
    
     "modified_by":null,
    
     "modified_date":"2023-04-05T06:50:59.517Z"
    
    },
    
    {
    
     "master_data_id":"4",
    
     "name":"TestObjectiver3",
    
     "description":"TestObjectiver3",
    
     "type":"OBJ3",
    
     "parent_master_id":"1",
    
     "is_delete":"N",
    
     "created_by":null,
    
     "created_date":"2023-04-05T06:51:19.189Z",
    
     "modified_by":null,
    
     "modified_date":"2023-04-05T06:51:19.189Z"
    
    },
    
    {
    
     "master_data_id":"5",
    
     "name":"TestObjectiver4",
    
     "description":"TestObjectiver4",
    
     "type":"OBJ4",
    
     "parent_master_id":"1",
    
     "is_delete":"N",
    
     "created_by":null,
    
     "created_date":"2023-04-05T06:51:35.355Z",
    
     "modified_by":null,
    
     "modified_date":"2023-04-05T06:51:35.355Z"
    
    }
    
    ]
    projectDropDownOptions:any[] = []
    campaignDropDownOptions:any[] = []
    timeZones = [
      { label: "Eastern Standard Time", value: "EST" },
      { label: "Central Standard Time", value: "CST" },
      { label: "Mountain Standard Time", value: "MST" },
      { label: "Pacific Standard Time", value: "PST" }
    ];
    campaignManager = [
      { label: "sample 1", value: "sample 1" },
      { label: "sample 2", value: "sample 2" },
      { label: "sample 3", value: "sample 3" },
      { label: "sample 4", value: "sample 4" }
    ];
     approver = [
      { label: "sample 1", value: "sample 1" },
      { label: "sample 2", value: "sample 2" },
      { label: "sample 3", value: "sample 3" },
      { label: "sample 4", value: "sample 4" }
    ];
    startMinDate = new Date()
    endMinDate = new Date()
    endDatePickerFlag = true
constructor(
  private fb : FormBuilder
){
}

ngOnInit(): void {
  this.projectDropDownOptions = JSON.parse(JSON.stringify(this.projectValueJson))
    this.projectDropDownOptions =  this.projectDropDownOptions.map((item:any)=>({
      label:item.company_name,
      value : item.configure_project_id
    }))
    this.campaignDropDownOptions = JSON.parse(JSON.stringify(this.campaignObjectiveValueJson))
    this.campaignDropDownOptions =  this.campaignDropDownOptions.map((item:any)=>({
      label:item.name,
      value : item.master_data_id
    }))
      console.log(this.endDatePickerFlag)
    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let date =  today.getUTCDate()
    console.log('month=>',month, 'year=>',year , "date=>", today.getUTCDate())
    let prevMonth = (month === 0) ? 11 : month -1;
    let prevYear = (prevMonth === 11) ? year - 1 : year;
    // let nextMonth = (month === 11) ? 0 : month + 1;
    // let nextYear = (nextMonth === 0) ? year + 1 : year;
    this.startMinDate.setMonth(month);
    this.startMinDate.setFullYear(year);
    this.startMinDate.setUTCDate(date)
  // console.log(this.minDate)
  
console.log(this.campaignDropDownOptions)
  
  this.campaignBuilderFormGroup = this.fb.group({
    project:['',[Validators.required]],
    name:['',[Validators.required]],
    description:['',[Validators.required]],
    objective:['',[Validators.required]],
    manager:['',[Validators.required]],
    approver:['',[Validators.required]],
    timelineStartDate:['',[Validators.required]],
    timelineEndDate:['',[Validators.required]],
    timezone:['',[Validators.required]],
    campaignMood:['',[Validators.required]]
  })
}
handleSaveChanges(){
  console.log(this.campaignBuilderFormGroup.value)

  this.endMinDate = this.startMinDate
  this.campaignBuilderFormGroup.reset()
}
handleDraftClick(){

}
handleDateOnchange(inputDate:Date){
  let today = new Date(inputDate);
  let month = today.getMonth();
  let year = today.getFullYear();
  let date =  today.getDate();
  this.endMinDate.setMonth(month)
  this.endMinDate.setFullYear(year)
  this.endMinDate.setUTCDate(date)
  this.endDatePickerFlag = false
  this.campaignBuilderFormGroup.controls['timelineEndDate'].setValue('')
}
}
