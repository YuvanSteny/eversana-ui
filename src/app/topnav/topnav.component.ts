import { Component } from '@angular/core';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent {

  imgUrl='https://m.economictimes.com/thumb/msid-91939341,width-1200,height-900,resizemode-4,imgsize-43522/tom-holland.jpg'

  configOverlayVisible:boolean = false  
  handleConfigClick(){ 
    this.configOverlayVisible = !this.configOverlayVisible
  }
  handleconfigMenuClick(){
    this.configOverlayVisible = !this.configOverlayVisible
  }

}
