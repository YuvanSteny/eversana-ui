import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageIntegrationComponent } from './manage-integration.component';

describe('ManageIntegrationComponent', () => {
  let component: ManageIntegrationComponent;
  let fixture: ComponentFixture<ManageIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageIntegrationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ManageIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
