import { Component , OnInit} from '@angular/core';
import { FormControl , FormGroup , FormArray , FormBuilder, Validators } from '@angular/forms';
import { BrowserTransferStateModule } from '@angular/platform-browser';
@Component({
  selector: 'app-manage-integration',
  templateUrl: './manage-integration.component.html',
  styleUrls: ['./manage-integration.component.scss']
})
export class ManageIntegrationComponent implements OnInit{

newConnFlag :boolean = false
  menuItems = [
    {label:'Snowflake (ACTICS)',state:true},
    {label:'Medscape',state:false},
    {label:'S3 Storage',state:false},
    {label:'Redshift',state:false}
  ]
  inComingData = [{
    name: "Snowflake (ACTICS)",
    secret_name: "redshift_dev_db",
    parameters: [
      {host: "127.0.0.1"},
      {user_name: "redshiftdb"},
      {port: "5432"}
    ]
   },
   {
    name: "Medscape",
    secret_name: "redshift_dev_db",
    parameters: [
      {host: "127.0.0.1"},
      {user_name: "redshiftdb"},
      {port: "5432"}
    ]
   },
   {
    name: "S3 Storage",
    secret_name: "redshift_dev_db",
    parameters: [
      {host: "127.0.0.1"},
      {user_name: "redshiftdb"},
      {port: "5432"}
    ]
   },
   {
    name: "Redshift",
    secret_name: "redshift_dev_db",
    parameters: [
      {host: "127.0.0.1"},
      {user_name: "redshiftdb"},
      {port: "5432"}
    ]
   },
  ]
    
  isSubmitted :boolean = false  
  constructor(
    private fb:FormBuilder
  ){

  }

  newConnFormGroup!:FormGroup 
  ngOnInit():void {
    this.newConnFormGroup = this.fb.group({
      name : ['',[Validators.required]],
      secret_name : ['',[Validators.required]],
      parameters :  this.fb.array([])
    })

  }

  get parameters(): FormArray {
    return this.newConnFormGroup.get('parameters') as FormArray;
  }

  handleSideNavClick(menu:any){

    this.menuItems.forEach((element,i)=>{
      if(element.label === menu.label){
        this.menuItems[i].state = true
      }else{
        this.menuItems[i].state = false
      }
      
    })
  }
  handleNewConnectionClick(){
    this.newConnFlag = true;
    this.newConnFormGroup.reset()
    this.parameters.clear()
    const obj = this.fb.group({
      key : ['',[Validators.required]],
      value : ['',[Validators.required]]
    })
    this.parameters.push(obj)
  }
  handleAddParameter(){
    const obj = this.fb.group({
      key : ['',[Validators.required]],
      value : ['',[Validators.required]]
    })

    this.parameters.push(obj)
  }
  handleRemoveParameter(i:number,control:any){
    console.log(this.newConnFormGroup.value)
    this.parameters.removeAt(i)
  }

  handleSaveChanges(){

    if( this.newConnFormGroup.valid){
      console.log( this.newConnFormGroup.value)
    }else{
      this.isSubmitted = true
    }
    
  }
  handleDeactivate(){

  }


  handleEditNewConnectionClick(){
    this.formValuePatch(this.inComingData[0])
  }

  formValuePatch(data:any){

    this.newConnFormGroup.patchValue({
      name:data.name,
      secret_name : data.secret_name
    })
    this.parameters.clear();
    data.parameters.forEach((element:any) => {
      let key = Object.keys(element)
      console.log(element)
      const obj = this.fb.group({
        key : [key,[Validators.required]],
        value : [element[key[0]],[Validators.required]]
      })
      this.parameters.push(obj)
    });
    this.newConnFlag = true
  }


}
